/* Simple generator of text files for the genetic program testing. */

#include <stdio.h>
#include <stdlib.h>

int
main(int argc, char *argv[])
{
	/* Test if we were executed properly. */
	if (argc != 4) {
		fprintf(stderr, "Usage: %s EQUATION_COUNT INPUT_FILE OUTPUT_FILE\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	/* Retrieve parameters. */
	int eqcount = atoi(argv[1]);
	FILE *infile = fopen(argv[2], "w");
	if (!infile) {
		perror(argv[2]);
		exit(EXIT_FAILURE);
	}
	FILE *outfile = fopen(argv[3], "w");
	if (!outfile) {
		perror(argv[2]);
		exit(EXIT_FAILURE);
	}

	/* Produce equations. */
	for (int i = 0; i < eqcount; i++) {
		/* Available operators. */
		static char ops[4] = "+-*/";

		/* Pick an operator and parameters. */
		int arg1 = rand() % 100; /* 0 to 99 */
		char op = ops[rand() % 4]; /* random operator character */
		int arg2 = rand() % 100; /* 0 to 99 */

		/* Make sure we do not divide by zero. */
		if (op == '/' && arg2 == 0)
			arg2 = 1 + rand() % 99; /* 1 to 99 */

		/* Write the equation. */
		fprintf(infile, "%d%c%d=\n", arg1, op, arg2);

		/* Figure out the result. */
		int result = 0;
		switch (op) {
			case '+': result = arg1 + arg2; break;
			case '-': result = arg1 - arg2; break;
			case '*': result = arg1 * arg2; break;
			case '/': result = arg1 / arg2; break;
		}

		/* Write the result. */
		fprintf(outfile, "%d\n", result);
	}
}
