CFLAGS=-std=gnu99 -g -Wall -Wno-switch
LIBS=-lgaul -lgaul_util

all: generator stage1 stage2

generator: generator.o
	$(CC) $(LDFLAGS) -o $@ $^

stage1: stage1.o
	$(CC) $(LDFLAGS) -o $@ $^ $(LIBS)

stage2: stage2.o
	$(CC) $(LDFLAGS) -o $@ $^ $(LIBS)

clean:
	rm -f *.o stage1 stage2 generator
